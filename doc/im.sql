-- 用户表
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account` varchar(64) NOT NULL COMMENT '账号',
  `nick` varchar(64) DEFAULT NULL COMMENT '昵称',
  `phone` varchar(64) DEFAULT NULL COMMENT '手机号',
  `role` varchar(64) DEFAULT 'normal' COMMENT '角色',
  `salt` char(10) NOT NULL COMMENT '盐值',
  `passwd` varchar(256) NOT NULL,
  `status` int(4) DEFAULT '1' COMMENT '状态：2:禁用,1:启用',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_account` (`account`),
  KEY `idx_create_time_status_account` (`create_time`,`status`,`account`),
  KEY `idx_status_create_time_account` (`status`,`create_time`,`account`),
  KEY `idx_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- 聊天房间表
CREATE TABLE `room` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `room_uuid` varchar(64) NOT NULL COMMENT '房间编号',
  `room_name` varchar(64) DEFAULT NULL COMMENT '房间名称',
  `status` int(4) DEFAULT '1' COMMENT '状态：1:启用,2:禁用',
  `owner` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_room_uuid` (`room_uuid`),
  KEY `idx_status_owner_create_time` (`status`,`owner`,`create_time`),
  KEY `idx_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='聊天房间表';