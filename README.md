# vaye-im-server

#### 项目介绍
基于`websocket`实现的简易的聊天服务端。

#### 功能概述
- [x] 点对点聊天（单机版）
- [x] 点对点聊天（集群版）
- [x] 群聊版


#### 安装教程

1.  将本项目拉取到本地
2.  修改配置文件（mysql、redis）
3.  启动项目

#### 使用说明

##### 点对点聊天测试

项目启动后，浏览器访问：http://127.0.0.1:7000/chat.html

![im_chat](https://gitee.com/vaye039/vaye-images/raw/master/vaye-im-server/im_chat.png)

###### 参数说明

- userId:发送消息用户id
- toUserId：接收消息用户Id
- contentTest：发送的消息内容

###### 操作步骤

- 填写userId、toUserId、contentTest
- 开启socket
- 发送消息
- 如果结束聊天就点击关闭socket断开连接

##### 群聊版

项目启动后，浏览器访问：http://127.0.0.1:7000/roomChat.html

![roomChatWeb](https://gitee.com/vaye039/vaye-images/raw/master/vaye-im-server/roomChatWeb.png)

###### 参数说明

- userId:发送消息用户id
- roomId：房间号Id
- contentTest：发送的消息内容

###### 操作步骤

- 填写userId、toUserId、contentTest
- 开启socket
- 发送消息
- 如果结束聊天就点击关闭socket断开连接

> 如果需要清楚消息记录，点击清空消息记录即可
