package com.vaye.common.enums;

/**
 * Topic类型枚举
 * @author wangzhiyong
 * @date 2023/1/19 下午2:42
 * @return null
 */
public enum TopicTypeEnums {
    PEER_TO_PEER(1, "点对点聊天"),
    GROUP_CHAT(2, "群聊"),
    MSG_PUSH(3, "系统推送");

    private Integer code;
    private String msg;

    private TopicTypeEnums(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
