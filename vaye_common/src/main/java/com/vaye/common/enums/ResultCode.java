package com.vaye.common.enums;

/**
 * 返回状态码定义
 * @author wangzhiyong
 * @date 2023/1/19 下午2:42
 * @return null
 */
public enum ResultCode {
    UNKNOWN(501, "服务器内部错误"),
    FAILED(201, "处理异常"),
    MISS_PARAMS(202, "缺少必要参数"),
    SUCCESS(200, "成功"),
    SYSTEM_ERROR(203, "系统异常");

    private Integer code;
    private String msg;

    private ResultCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
