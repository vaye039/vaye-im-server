package com.vaye.common.utils;

/**
 * 请求耗时统计
 *
 * @author wangzhiyong
 * @module admin
 * @date 2022年12月29日 下午5:53
 */
public class AccessTimeHolder {

    private static ThreadLocal<Long> accessTimeLocal = new ThreadLocal<>();

    public static void start(){
        accessTimeLocal.set(System.currentTimeMillis());
    }

    public static long end(){
        Long startTime = accessTimeLocal.get();
        long costTime = System.currentTimeMillis() - startTime;
        accessTimeLocal.remove();
        return costTime;
    }
}
