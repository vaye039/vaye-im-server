package com.vaye.common.utils;

import cn.hutool.dfa.FoundWord;
import cn.hutool.dfa.SensitiveUtil;
import org.slf4j.MDC;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

/**
 * MDC工具类
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年01月03日 上午10:26
 */
public class MDCUtils {

    private static final String TRACE_ID = "traceId";

    /**
     * 添加链路追踪ID
     * @author wangzhiyong
     * @date 2023/1/3 上午10:28
     * @param httpServletRequest
     */
    public static void putTraceId(HttpServletRequest httpServletRequest){
        String traceId = httpServletRequest.getHeader(TRACE_ID);
        if (StringUtils.isEmpty(traceId)) {
            MDC.put(TRACE_ID, UuidUtils.getUUIDStr());
        } else {
            MDC.put(TRACE_ID, traceId);
        }
    }

    /**
     * 清楚链路追踪ID
     * @author wangzhiyong
     * @date 2023/1/3 上午10:30
     */
    public static void clearTraceId(){
        MDC.remove(TRACE_ID);
    }

}
