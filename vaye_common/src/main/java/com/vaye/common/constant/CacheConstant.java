package com.vaye.common.constant;

/**
 * @author wangzhiyong
 * @date 2022年07月11日 下午2:51
 */
public class CacheConstant {

    //聊天消息主题
    public final static String WS_CHAT_TOPIC = "ws_chat_topic";

    //推送主题
    public final static String WS_PUSH_TOPIC = "ws_push_topic";

    //在线人数
    public final static String ONLINE_COUNT = "onlineCount";

    //在线用户列表
    public final static String ONLINE_USERS = "onlineUsers";

    //房间在线用户列表
    public final static String ONLINE_ROOM_USERS_FORMAT = "onlineRoomUsers_%s";
}
