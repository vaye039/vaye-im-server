package com.vaye.common.constant;

/**
 * IM常量类
 * @author wangzhiyong
 * @date 2022年07月11日 下午2:51
 */
public class ImConstant {

    //用户名
    public final static String USER_NAME = "userName";

    public final static String LOGINID = "loginId";

    //请求host
    public final static String HOST = "host";

    //请求token
    public final static String TOKEN = "token";

    //token分隔符
    public final static String SEPARATE_TOKEN = "_";

    public final static String TOKEN_FORMAT = "%d_%s_%s_%s";

}
