package com.vaye.common.base;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 分类名称
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年05月31日 下午4:56
 */
@Data
public class BaseModel extends AbstractModel{

    //主键
    @TableId(type = IdType.AUTO)
    private Long id;
}
