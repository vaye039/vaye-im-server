package com.vaye.common.base;

import com.vaye.common.enums.ResultCode;
import lombok.Data;

import java.io.Serializable;

/**
 * 项目名称：xcy-integral
 * 类 名 称：ResultBase
 * 类 描 述：TODO
 * 创建时间：2020/7/6 5:29 PM
 * 创 建 人：wangzhiyong
 */
@Data
public class ResultBase<T> implements Serializable {

    private static final long serialVersionUID = -6052123757147763865L;

    //code码
    private Integer code;

    //错误信息
    private String msg;
    //数据
    private T data;
    public ResultBase() {
        super();
    }

    public ResultBase(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    /** 结果集已知错误实例化并设置错误信息 */
    public static ResultBase<?> error(Integer code, String msg) {
        ResultBase<?> resultBean = new ResultBase<>();
        resultBean.setCode(code);
        resultBean.setMsg(msg);
        return resultBean;
    }

    public static ResultBase<?> error() {
        return error(ResultCode.SYSTEM_ERROR);
    }

    public static ResultBase<?> error(ResultCode returnCode) {
        return error(returnCode.getCode(), returnCode.getMsg());
    }

    @SuppressWarnings("unchecked")
    public static <T> ResultBase<T> success() {
        ResultBase resultBase = new ResultBase();
        resultBase.setCode(ResultCode.SUCCESS.getCode());
        resultBase.setMsg(ResultCode.SUCCESS.getMsg());
        return resultBase;
    }

    @SuppressWarnings("unchecked")
    public static <T> ResultBase<T> success(Object data) {
        ResultBase resultBase = new ResultBase();
        resultBase.setCode(ResultCode.SUCCESS.getCode());
        resultBase.setMsg(ResultCode.SUCCESS.getMsg());
        resultBase.setData(data);
        return resultBase;
    }

    public static <T> ResultBase<T> fail() {
        return fail(ResultCode.FAILED.getCode(),ResultCode.FAILED.getMsg());
    }

    @SuppressWarnings("unchecked")
    public static <T> ResultBase<T> fail(String errorMsg) {
        return fail(ResultCode.FAILED.getCode(),errorMsg);
    }

    @SuppressWarnings("unchecked")
    public static <T> ResultBase<T> fail(Integer errorCode, String errorMsg) {
        ResultBase resultBase = new ResultBase();
        resultBase.setCode(errorCode);
        resultBase.setMsg(errorMsg);
        return resultBase;
    }

    @SuppressWarnings("unchecked")
    public static <T> ResultBase<T> fail(Integer errorCode, String errorMsg, Object data) {
        ResultBase resultBase = new ResultBase();
        resultBase.setCode(errorCode);
        resultBase.setMsg(errorMsg);
        resultBase.setData(data);
        return resultBase;
    }

}
