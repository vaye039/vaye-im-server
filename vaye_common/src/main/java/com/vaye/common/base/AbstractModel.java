package com.vaye.common.base;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.awt.*;
import java.io.Serializable;

/**
 * 分类名称
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年05月31日 下午4:56
 */
public abstract class AbstractModel implements Serializable {

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
