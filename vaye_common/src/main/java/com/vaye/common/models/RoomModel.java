package com.vaye.common.models;

import com.baomidou.mybatisplus.annotation.TableName;
import com.vaye.common.base.BaseModel;
import lombok.Data;

import java.util.Date;

/**
 * 房间实体类
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年05月31日 下午4:59
 */
@Data
@TableName(value = "room")
public class RoomModel extends BaseModel {

    //房间编号
    private String roomUuid;
    //房间名称
    private String roomName;

    //创建人
    private String owner;
    //状态
    private String status;
    //创建时间
    private Date createTime;

    //修改时间
    private Date gmtModified;
}
