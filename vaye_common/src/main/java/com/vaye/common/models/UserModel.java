package com.vaye.common.models;

import com.baomidou.mybatisplus.annotation.TableName;
import com.vaye.common.base.BaseModel;
import lombok.Data;

import java.util.Date;

/**
 * 分类名称
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年05月31日 下午4:59
 */
@Data
@TableName(value = "user")
public class UserModel extends BaseModel {

    private String account;

    private String nick;

    private String phone;

    private String role;

    private String passwd;

    private String salt;

    private String status;

    private Date createTime;
}
