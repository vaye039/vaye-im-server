package com.vaye.im;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication(scanBasePackages = {"com.vaye.**"})
@MapperScan("com.vaye.im.dao")
public class VayeImServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(VayeImServerApplication.class, args);
    }

}
