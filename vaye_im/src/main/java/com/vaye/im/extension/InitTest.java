package com.vaye.im.extension;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 分类名称
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年06月30日 下午4:36
 */
@Component
public class InitTest implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) throws Exception {
    }
}
