package com.vaye.im.extension;

import com.vaye.common.base.ResultBase;
import com.vaye.common.enums.ResultCode;

/**
 * 分类名称
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年06月28日 下午2:20
 */
public class CustomException extends RuntimeException{

    private Integer code;

    private String msg;

    public CustomException(ResultBase error) {
        super(error.getMsg());
        this.code = error.getCode();
        this.msg = error.getMsg();
    }

    public CustomException(String msg) {
        super(msg);
        this.code = ResultCode.FAILED.getCode();
        this.msg = msg;
    }
}
