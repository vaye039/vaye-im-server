package com.vaye.im.extension;

import com.vaye.common.base.ResultBase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 分类名称
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年06月28日 下午2:18
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    /**
     *  处理系统异常
     * @author wangzhiyong
     * @date 2022/11/7 上午11:21
     * @param e
     * @return com.sw.swtransferserver.common.ResultModel
     */
    @ExceptionHandler(Exception.class)
    public ResultBase handleProcessException(Exception e){
        log.error("Exception=系统异常",e.getMessage(),e);
        e.printStackTrace();
        return ResultBase.fail(e.getMessage());
    }

    /**
     * 处理自定义异常
     * @author wangzhiyong
     * @date 2023/1/3 下午5:50
     * @param e
     * @return com.shinewonder.commons.utils.R
     */
    @ExceptionHandler(CustomException.class)
    public ResultBase handCustomException(CustomException e){
        log.warn("[自定义异常]:{}",e.getMessage());
        return ResultBase.fail(e.getMessage());
    }
}
