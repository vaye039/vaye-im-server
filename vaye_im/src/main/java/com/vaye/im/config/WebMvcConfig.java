package com.vaye.im.config;

import com.vaye.im.intercepter.GlobalInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

/**
 * 分类名称
 *
 * @author wangzhiyong
 * @module admin
 * @date 2022年12月21日 下午5:44
 */
@Configuration
@Slf4j
public class WebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private GlobalInterceptor globalInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        log.info("init GlobalInterceptor");
        registry.addInterceptor(globalInterceptor).addPathPatterns("/**").excludePathPatterns(
                "/**/*.html",
                "/**/*.ico",
                "/error",
                "/excute",   //健康检查接口
                "/im/user/*"  //用户操作相关接口
        );
    }

    /**
     * 配置静态页面
     * @author wangzhiyong
     * @date 2023/6/20 上午8:44
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/roomChat").setViewName("roomChat");
        registry.addViewController("/chat").setViewName("chat");
        registry.addViewController("/upload1").setViewName("upload1");
        registry.addViewController("/upload2").setViewName("upload2");
    }
}
