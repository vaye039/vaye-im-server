package com.vaye.im.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 消息实体类
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年07月07日 上午10:41
 */
@Data
public class MessageDTO implements Serializable {
    private static final long serialVersionUID = -781224587408983328L;

    //消息状态码
    private Integer code;

    //消息类型：1-点对点聊天消息、2-群消息、3-系统推送
    private Integer type = 0;

    //消息来源用户ID
    private Long fromUserId;

    //消息目标用户ID
    private Long targetUserId;

    //房间号
    private String roomId;

    //消息
    private String msg;
}
