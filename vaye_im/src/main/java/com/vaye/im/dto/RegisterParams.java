package com.vaye.im.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 分类名称
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年05月31日 下午4:31
 */
@Data
public class RegisterParams implements Serializable {
    private static final long serialVersionUID = -2446574900624461473L;

    private String account;

    private String nick;

    private String phone;

    private String passwd;


}
