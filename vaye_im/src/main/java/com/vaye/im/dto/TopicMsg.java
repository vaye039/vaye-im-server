package com.vaye.im.dto;

import com.vaye.common.enums.TopicTypeEnums;
import lombok.Data;

/**
 * @author wangzhiyong
 * @date 2022年07月11日 下午2:52
 */
@Data
public class TopicMsg {

    /**
     * 消息类型：{@link TopicTypeEnums}
     */
    private Integer type;

    //房间号
    private String roomId;

    //发送消息人ID
    private String fromUserId;

    //接收消息人ID
    private String targetUserId;

    //消息内容
    private String msg;

    //ip
    private String ip;
}
