package com.vaye.im.dto;

import lombok.Data;

/**
 * @author wangzhiyong
 * @date 2022年07月11日 下午2:52
 */
@Data
public class UserInfo {

    private Long userId;

    private String userName;
}
