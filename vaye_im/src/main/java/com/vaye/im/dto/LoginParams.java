package com.vaye.im.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 登录请求参数
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年05月31日 下午4:31
 */
@Data
public class LoginParams implements Serializable {

    private static final long serialVersionUID = 4343575718771598890L;
    private String account;

    private String passwd;

    private String device;


}
