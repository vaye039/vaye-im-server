package com.vaye.im.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 分类名称
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年06月07日 下午4:36
 */
@Data
public class UseroutParams  implements Serializable {

    private static final long serialVersionUID = -8053607114989851015L;

    //设备
    private String device;

    //用户ID
    private Long id;

    //登录token
    private String token;
}
