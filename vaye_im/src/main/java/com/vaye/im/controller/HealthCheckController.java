package com.vaye.im.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 健康检查
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年01月18日 下午4:15
 */
@RestController
@Slf4j
public class HealthCheckController {

    @GetMapping("excute")
    public String excute(){
        return "ok";
    }
}
