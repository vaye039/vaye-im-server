package com.vaye.im.controller;

import com.alibaba.fastjson.JSON;
import com.vaye.common.base.ResultBase;
import com.vaye.common.constant.CacheConstant;
import com.vaye.common.enums.TopicTypeEnums;
import com.vaye.im.dto.TopicMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * IM管理控制器
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年01月19日 下午2:33
 */
@RestController
@RequestMapping("/im/manager")
public class ImManagerController {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 指定用户消息推送
     * @author wangzhiyong
     * @date 2023/1/19 下午3:05
     * @param userId
     * @param msg
     * @return com.vaye.common.base.ResultBase
     */
    @PostMapping("pushSingle")
    public ResultBase pushSingle(@RequestParam Long userId,@RequestParam String msg){
        TopicMsg topicMsg = new TopicMsg();
        topicMsg.setType(TopicTypeEnums.MSG_PUSH.getCode());
        topicMsg.setTargetUserId(userId + "");
        topicMsg.setMsg(msg);
        String body = JSON.toJSONString(topicMsg);
        stringRedisTemplate.convertAndSend(CacheConstant.WS_PUSH_TOPIC,body);
        return ResultBase.success();
    }

    /**
     * 所有在线用户消息推送
     * @author wangzhiyong
     * @date 2023/1/19 下午3:05
     * @param msg
     * @return com.vaye.common.base.ResultBase
     */
    @PostMapping("pushAll")
    public ResultBase pushAll(@RequestParam String msg){
        TopicMsg topicMsg = new TopicMsg();
        topicMsg.setType(TopicTypeEnums.MSG_PUSH.getCode());
        topicMsg.setMsg(msg);
        String body = JSON.toJSONString(topicMsg);
        stringRedisTemplate.convertAndSend(CacheConstant.WS_PUSH_TOPIC,body);
        return ResultBase.success();
    }
}
