package com.vaye.im.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.vaye.common.base.ResultBase;
import com.vaye.im.dto.LoginParams;
import com.vaye.im.dto.RegisterParams;
import com.vaye.im.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * IM用户控制器
 *
 * @author wangzhiyong
 * @module im
 * @date 2023/5/31 下午2:33
 */
@RestController
@RequestMapping("/im/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 用户注册
     * @author wangzhiyong
     * @date 2023/5/31 下午4:34
     * @param params 注册参数
     * @return com.vaye.common.base.ResultBase
     */
    @PostMapping("register")
    public ResultBase register(@RequestBody RegisterParams params){
        if (ObjectUtils.isEmpty(params)) {
            ResultBase.fail("the params of request is empty");
        }
        return userService.register(params);
    }

    /**
     * 用户登录
     * @author wangzhiyong
     * @date 2023/6/7 下午3:56
     * @param params
     * @return com.vaye.common.base.ResultBase
     */
    @PostMapping("login")
    public ResultBase login(@RequestBody LoginParams params){
        if (ObjectUtils.isEmpty(params)) {
            ResultBase.fail("the params of request is empty");
        }
        return userService.login(params);
    }

    /**
     * 判断用户是否登录
     * @author wangzhiyong
     * @date 2023/6/7 下午3:56
     * @return com.vaye.common.base.ResultBase
     */
    @RequestMapping("isLogin")
    public ResultBase isLogin() {
        return ResultBase.success("是否登录：" + StpUtil.isLogin());
    }

    /**
     * 查询token信息
     * @author wangzhiyong
     * @date 2023/6/7 下午3:57
     * @return com.vaye.common.base.ResultBase
     */
    @RequestMapping("tokenInfo")
    public ResultBase tokenInfo() {
        return ResultBase.success(StpUtil.getTokenInfo());
    }

    /**
     * 用户退出登录
     * @author wangzhiyong
     * @date 2023/6/7 下午3:58
     * @return com.vaye.common.base.ResultBase
     */
    @RequestMapping("logout")
    public ResultBase logout() {
        StpUtil.logout();
        return ResultBase.success();
    }

}
