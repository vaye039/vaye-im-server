package com.vaye.im.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.vaye.common.base.ResultBase;
import com.vaye.im.dao.RoomMapper;
import com.vaye.im.dto.LoginParams;
import com.vaye.im.dto.RegisterParams;
import com.vaye.im.service.RoomService;
import com.vaye.im.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * 房间管理
 * @author wangzhiyong
 * @module im
 * @date 2023/5/31 下午2:33
 */
@RestController
@RequestMapping("/im/room")
@Slf4j
public class RoomController {

    @Autowired
    private RoomService roomService;

    /**
     * 创建房间
     * @author wangzhiyong
     * @date 2023/6/28 下午2:27
     * @param roomName 房间名称
     * @return com.vaye.common.base.ResultBase
     */
    @PostMapping("/create")
    public ResultBase create(@RequestParam("roomName") String roomName){
        log.info("开始创建房间，roomName={}",roomName);
        if (StringUtils.isEmpty(roomName)) {
            ResultBase.fail("the roomName of request is not empty");
        }
        return roomService.create(roomName);
    }

}
