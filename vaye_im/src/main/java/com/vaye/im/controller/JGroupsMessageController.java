package com.vaye.im.controller;

import com.vaye.common.base.ResultBase;
import com.vaye.im.extension.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * JGroups消息控制器
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年07月13日 上午11:03
 */
@RestController
@RequestMapping("/jgroups")
public class JGroupsMessageController {

    @Autowired
    private Node node;

    /**
     * 发送消息
     * @author wangzhiyong
     * @date 2023/7/13 上午11:14
     * @param msg
     * @return com.vaye.common.base.ResultBase
     */
    @GetMapping("/send")
    public ResultBase send(@RequestParam("msg") String msg){
        node.senMsg(null,msg);
        return ResultBase.success();
    }
}
