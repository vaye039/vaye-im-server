package com.vaye.im.controller;

import cn.dev33.satoken.session.SaSession;
import cn.dev33.satoken.stp.StpUtil;
import com.vaye.common.base.ResultBase;
import com.vaye.im.dto.UseroutParams;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * IM用户控制器
 *
 * @author wangzhiyong
 * @module im
 * @date 2023/5/31 下午2:33
 */
@RestController
@RequestMapping("/im/userManager")
public class UserManagerController {

    /**
     * 踢人下线
     * @author wangzhiyong
     * @date 2023/6/7 下午3:57
     * @param params {@link UseroutParams}
     * @return com.vaye.common.base.ResultBase
     */
    @RequestMapping("kickout")
    public ResultBase kickout(@RequestBody UseroutParams params) {
        if (ObjectUtils.isEmpty(params)) {
            ResultBase.fail("请求参数为空");
        }
        if (!StringUtils.isEmpty(params.getToken())) {
            StpUtil.kickoutByTokenValue(params.getToken());
        } else if (!ObjectUtils.isEmpty(params.getId()) && !StringUtils.isEmpty(params.getDevice())) {
            StpUtil.kickout(params.getId(), params.getDevice());
        } else {
            StpUtil.kickout(params.getId());
        }
        return ResultBase.success("下线成功");
    }

    /**
     * 强制注销
     * @author wangzhiyong
     * @date 2023/6/7 下午5:22
     * @param params
     * @return com.vaye.common.base.ResultBase
     */
    @RequestMapping("logout")
    public ResultBase logout(@RequestBody UseroutParams params) {
        if (ObjectUtils.isEmpty(params)) {
            ResultBase.fail("请求参数为空");
        }
        if (!StringUtils.isEmpty(params.getToken())) {
            StpUtil.logoutByTokenValue(params.getToken());
        } else if (!ObjectUtils.isEmpty(params.getId()) && !StringUtils.isEmpty(params.getDevice())) {
            StpUtil.logout(params.getId(), params.getDevice());
        } else {
            StpUtil.logout(params.getId());
        }
        StpUtil.disable(1,3600);
        StpUtil.checkDisable(1);
        return ResultBase.success("用户注销成功");
    }

    /**
     *  获取登录用户信息
     * @author wangzhiyong
     * @date 2023/6/13 上午10:35
     * @param id 登录人id
     * @return com.vaye.common.base.ResultBase
     */
    @GetMapping("getUserInfo")
    public ResultBase getUserInfo(@RequestParam(value = "id" ,required = false) Long id){
        SaSession saSession;
        if (ObjectUtils.isEmpty(id)) {
            saSession = StpUtil.getSession();
        } else{
            saSession = StpUtil.getSessionByLoginId(id);
        }
        if (ObjectUtils.isEmpty(saSession)) {
            return ResultBase.fail("该用户不在线");
        }
        return ResultBase.success(saSession.getDataMap());
    }

}
