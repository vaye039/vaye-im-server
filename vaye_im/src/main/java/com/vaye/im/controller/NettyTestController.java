package com.vaye.im.controller;

import com.vaye.im.netty.NettyServer;
import com.vaye.im.service.NettyPushMsgService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 分类名称
 *
 * @author wangzhiyong
 * @module admin
 * @date 2024年04月25日 上午10:20
 */
@RestController
@RequestMapping("/push")
public class NettyTestController {

    @Autowired
    private NettyPushMsgService pushMsgService;

    @PostMapping("/msg")
    public String pushMsg(@RequestParam("userId") String userId,@RequestParam("msg") String msg){
        if (StringUtils.isEmpty(userId)) {
            pushMsgService.pushMsgToAll(msg);
        } else {
            pushMsgService.pushMsgToOne(userId,msg);
        }
        return "success";
    }
}
