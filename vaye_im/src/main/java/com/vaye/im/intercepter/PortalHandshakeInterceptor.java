package com.vaye.im.intercepter;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Objects;
/**
 * webSocket握手拦截器
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年01月19日 下午5:49
 */
@Component
@Slf4j
public class PortalHandshakeInterceptor implements HandshakeInterceptor {


    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Map<String, Object> map) throws Exception {
        log.info("HandshakeInterceptor beforeHandshake start...");
        if (request instanceof ServletServerHttpRequest) {
            HttpServletRequest req = ((ServletServerHttpRequest) request).getServletRequest();
            String authorization = req.getHeader("Sec-WebSocket-Protocol");
            log.info("authorization = {}", authorization);
            if (StringUtils.isEmpty(authorization) || !"123".equals(authorization)) {
                serverHttpResponse.setStatusCode(HttpStatus.FORBIDDEN);
                log.info("【beforeHandshake】 authorization Parse failure. authorization = {}", authorization);
                return false;
            }
            //存入数据，方便在hander中获取，这里只是在方便在webSocket中存储了数据，并不是在正常的httpSession中存储，想要在平时使用的session中获得这里的数据，需要使用session 来存储一下
//            map.put(MagicCode.WEBSOCKET_USER_ID, userId);
//            map.put(MagicCode.WEBSOCKET_CREATED, System.currentTimeMillis());
//            map.put(MagicCode.TOKEN, authorization);
//            map.put(HttpSessionHandshakeInterceptor.HTTP_SESSION_ID_ATTR_NAME, req.getSession().getId());
            log.info("【beforeHandshake】 WEBSOCKET_INFO_MAP: {}", map);
        }
        log.info("HandshakeInterceptor beforeHandshake end...");
        return true;
    }

    @Override
    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Exception e) {
        log.info("HandshakeInterceptor afterHandshake start...");
//        HttpServletRequest httpRequest = ((ServletServerHttpRequest) request).getServletRequest();
//        HttpServletResponse httpResponse = ((ServletServerHttpResponse) serverHttpResponse).getServletResponse();
//        if (StringUtils.isNotEmpty(httpRequest.getHeader("Sec-WebSocket-Protocol"))) {
//            httpResponse.addHeader("Sec-WebSocket-Protocol", httpRequest.getHeader("Sec-WebSocket-Protocol"));
//        }
        log.info("HandshakeInterceptor afterHandshake end...");
    }
}
