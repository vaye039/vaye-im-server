package com.vaye.im.websocket;

import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.vaye.common.constant.CacheConstant;
import com.vaye.common.utils.RedisUtil;
import com.vaye.common.utils.SpringContextUtils;
import com.vaye.im.dto.TopicMsg;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 点对点聊天(集群版)
 * PS：该类已经作废，如果使用的话请参照优化后的版本：{@link ProductWebSocket21}
 * @author wangzhiyong
 * @date 2022年04月04日 上午8:24
 * 1） value = "/ws/{userId}"
 * onOpen(@PathParam("userId") String userId, Session session){ // ... }
 * 这种方式必须在前端在/后面拼接参数 ws://localhost:7889/productWebSocket/123 ，否则404
 *
 * 2） value = "/ws"
 * onOpen(Session session){ // ... }
 * Map<String, List<String>> requestParameterMap = session.getRequestParameterMap();
 * // 获得 ?userId=123 这样的参数
 * @author bart
 */
@Slf4j
@Component
@ServerEndpoint(value = "/im2/{userId}/{targetUserId}") //添加消息编码器
@Deprecated
public class ProductWebSocket2 {

    private StringRedisTemplate stringRedisTemplate = SpringContextUtils.getBean(StringRedisTemplate.class);

    private static RedisUtil redisUtil;

    @Autowired
    public void setRedisUtil(RedisUtil redisUtil) {
        ProductWebSocket2.redisUtil = redisUtil;
    }

    // 当前登录用户的id和websocket session的map
    private static ConcurrentHashMap<String, Session> userIdSessionMap = new ConcurrentHashMap<>();

    @OnOpen
    public void onOpen(@PathParam("userId") String userId, Session session){
        if (ObjectUtils.isEmpty(userId)) {
            log.error("websocket连接 缺少参数 id");
            throw new IllegalArgumentException("websocket连接 缺少参数 id");
        }
        log.info("websocket 新客户端连入，用户id：{},",userId);
        userIdSessionMap.put(userId, session);
        addOnlineCount();
        log.info("当前在线人数：{}人",getOnlineCount());
        // 发送消息返回当前用户
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", 200);
        jsonObject.put("message", "OK");
        send(null,userId, JSON.toJSONString(jsonObject));
    }

    /**
     * 服务端接收到信息后调用
     *
     * @param message
     * @param session
     */
    @OnMessage
    public void onMessage(@PathParam("userId") String userId,@PathParam("targetUserId") String targetUserId, String message, Session session) {
        log.info("用户发送过来的消息为：" + message);
        try {
            if (StringUtils.isEmpty(targetUserId)) {
                session.getBasicRemote().sendText(message);
                return;
            }
            if (userIdSessionMap.containsKey(targetUserId)) {
                send(userId,targetUserId,message);
            } else {
                TopicMsg topicMsg = new TopicMsg();
                topicMsg.setFromUserId(userId);
                topicMsg.setTargetUserId(targetUserId);
                topicMsg.setMsg(message);
                String body = JSON.toJSONString(topicMsg);
                stringRedisTemplate.convertAndSend(CacheConstant.WS_PUSH_TOPIC,body);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 连接关闭时调用
     */
    @OnClose
    public void onClose(@PathParam("userId") String userId, Session session) {
        log.info("一个客户端关闭连接,客户端userId={}",userId);
        userIdSessionMap.remove(userId);
        subOnlineCount();
        log.info("当前在线人数：{}",getOnlineCount());
    }

    /**
     * 服务端websocket出错时调用
     *
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        log.error("websocket出现错误");
        error.printStackTrace();
    }

    public static synchronized void subOnlineCount() {
        redisUtil.decr(CacheConstant.ONLINE_COUNT,1L);
    }

    public static synchronized int getOnlineCount() {
        return (int)redisUtil.get(CacheConstant.ONLINE_COUNT);
    }

    /**
     * 服务端发送信息给客户端
     * @param fromUserId 发送用户ID
     * @param targetUserId 用户ID
     * @param message 发送的消息
     */
    public void send(String fromUserId,String targetUserId, String message) {
        log.info("#### 点对点消息，userId={}", targetUserId);
        if (MapUtil.isEmpty(userIdSessionMap)) {
            log.warn("当前无websocket连接");
            return;
        }
        if (!StringUtils.isEmpty(fromUserId)) {
            message = String.format("%s:%s",fromUserId,message);
        }
        if (StringUtils.isEmpty(targetUserId)) {  //群发
            String finalMessage = message;
            userIdSessionMap.forEach((k, v) ->{
                try {
                    v.getBasicRemote().sendText(finalMessage);//发送string
                    log.info("推送用户【{}】消息成功，消息为：【{}】", k , finalMessage);
                } catch (IOException e) {
                    e.printStackTrace();
                    log.error("推送用户【{}】消息失败，消息为：【{}】，原因是：【{}】", targetUserId , finalMessage, e.getMessage());
                }

            });
        }else {
            if (!userIdSessionMap.containsKey(targetUserId)) {
                log.warn("userId={}不在线",targetUserId);
                return;
            }
            Session session = userIdSessionMap.get(targetUserId);
            try {
                session.getBasicRemote().sendText(message);//发送string
                log.info("推送用户【{}】消息成功，消息为：【{}】", targetUserId , message);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("推送用户【{}】消息失败，消息为：【{}】，原因是：【{}】", targetUserId , message, e.getMessage());
            }

        }
    }

    public static synchronized void addOnlineCount() {
        redisUtil.incr(CacheConstant.ONLINE_COUNT,1L);
    }
}
