package com.vaye.im.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vaye.common.models.UserModel;

/**
 * @author wangzhiyong
 * @date 2023年05月31日 下午5:02
 */
public interface UserMapper extends BaseMapper<UserModel> {

}
