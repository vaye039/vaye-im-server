package com.vaye.im.service;

import com.vaye.common.base.ResultBase;
import com.vaye.im.dto.LoginParams;
import com.vaye.im.dto.RegisterParams;

/**
 * @author wangzhiyong
 * @date 2023年05月31日 下午4:40
 */
public interface UserService {
    ResultBase register(RegisterParams params);

    ResultBase login(LoginParams params);
}
