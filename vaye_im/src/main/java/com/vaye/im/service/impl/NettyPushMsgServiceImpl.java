package com.vaye.im.service.impl;

import com.vaye.im.netty.NettyConfig;
import com.vaye.im.service.NettyPushMsgService;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * 分类名称
 *
 * @author wangzhiyong
 * @module admin
 * @date 2024年04月25日 上午10:21
 */
@Service
public class NettyPushMsgServiceImpl implements NettyPushMsgService {
    @Override
    public void pushMsgToOne(String userId, String msg) {
        Channel channel = NettyConfig.getChannel(userId);
        if (Objects.isNull(channel)) {
            throw new RuntimeException("未连接socket服务器");
        }

        channel.writeAndFlush(new TextWebSocketFrame(msg));
    }

    @Override
    public void pushMsgToAll(String msg) {
        NettyConfig.getChannelGroup().writeAndFlush(new TextWebSocketFrame(msg));
    }
}
