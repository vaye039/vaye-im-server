package com.vaye.im.service;

/**
 * @author wangzhiyong
 * @date 2024年04月25日 上午10:21
 */
public interface NettyPushMsgService {
    //推送给指定用户
    void pushMsgToOne(String userId,String msg);

    //推送给所有用户
    void pushMsgToAll(String msg);
}
