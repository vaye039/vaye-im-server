package com.vaye.im.service.impl;

import com.vaye.common.base.ResultBase;
import com.vaye.common.models.RoomModel;
import com.vaye.common.utils.UserContextHolder;
import com.vaye.common.utils.UuidUtils;
import com.vaye.im.dao.RoomMapper;
import com.vaye.im.service.RoomService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 分类名称
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年06月28日 下午1:59
 */
@Service
public class RoomServiceImpl implements RoomService {

    @Resource
    private RoomMapper roomMapper;

    @Override
    public ResultBase create(String roomName) {
        RoomModel roomModel = new RoomModel();
        roomModel.setRoomUuid(UuidUtils.getUUID());
        roomModel.setRoomName(roomName);
        roomModel.setOwner(UserContextHolder.getUserName());
        roomMapper.insert(roomModel);
        return ResultBase.success();
    }
}
