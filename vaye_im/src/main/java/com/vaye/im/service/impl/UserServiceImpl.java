package com.vaye.im.service.impl;

import cn.dev33.satoken.secure.SaSecureUtil;
import cn.dev33.satoken.session.SaSession;
import cn.dev33.satoken.stp.SaLoginModel;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.PhoneUtil;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.vaye.common.base.ResultBase;
import com.vaye.common.models.UserModel;
import com.vaye.common.utils.DigestUtil;
import com.vaye.im.dao.UserMapper;
import com.vaye.im.dto.LoginParams;
import com.vaye.im.dto.RegisterParams;
import com.vaye.im.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * 分类名称
 *
 * @author wangzhiyong
 * @module admin
 * @date 2023年05月31日 下午4:40
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public ResultBase register(RegisterParams params) {
        try {
            Map<String, Object> columnMap = Maps.newHashMap();
            columnMap.put("account",params.getAccount());
            List<UserModel> userModels = userMapper.selectByMap(columnMap);
            if (!CollectionUtils.isEmpty(userModels)) {
                return ResultBase.fail("账户名已存在");
            }
            String password = DigestUtil.encryptPassword(params.getPasswd());
            String salt = RandomUtil.randomString(6);
            String pwd = SaSecureUtil.md5BySalt(params.getPasswd(), salt);
            UserModel userModel =new UserModel();
            userModel.setAccount(params.getAccount());
            userModel.setNick(params.getNick());
            userModel.setPhone(params.getPhone());
            userModel.setRole("normal");
            userModel.setSalt(salt);
            userModel.setPasswd(pwd);
            userMapper.insert(userModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultBase.success();
    }

    @Override
    public ResultBase login(LoginParams params) {
        try {
            Map<String, Object> columnMap = Maps.newHashMap();
            if (PhoneUtil.isMobile(params.getAccount())) {
                columnMap.put("phone",params.getAccount());
            }else {
                columnMap.put("account",params.getAccount());
            }
            List<UserModel> userModels = userMapper.selectByMap(columnMap);
            if (CollectionUtils.isEmpty(userModels)) {
                return ResultBase.fail("用户名｜密码输入有误");
            }
            UserModel userModel = userModels.get(0);
            if (!userModel.getPasswd().equals(SaSecureUtil.md5BySalt(params.getPasswd(), userModel.getSalt()))) {
                return ResultBase.fail("用户名｜密码输入有误");
            }
            if (StringUtils.isEmpty(params.getDevice())) {
                StpUtil.login(userModel.getId());
            }else {
                StpUtil.login(userModel.getId(),params.getDevice());
            }
            JSONObject userInfo = new JSONObject();
            userInfo.put("id",userModel.getId());
            userInfo.put("account",userModel.getAccount());
            userInfo.put("nick",userModel.getNick());

            StpUtil.getSession().set(SaSession.USER,userInfo);
            return ResultBase.success("登录成功");
        } catch (Exception e) {
            log.error("登录失败");
            e.printStackTrace();
            return ResultBase.error();
        }
    }
}
